import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0

Page {
    id: page

    ConfigurationGroup {
        id: conf
        path: "/org/nephros/openrepos-djvulibre/mime-helper"
        property bool enabled: false
    }

    DBusInterface {
        id: systemd

        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        signalsEnabled: true


        function runService() {
            systemd.typedCall("StartUnit", [ 
              {"type": "s", "value": "openrepos-djvulibre-open-installer.service"},
              {"type": "s", "value": "fail"}
              ],
              function(result) { console.log('Systemd call result: ' + result) },
              function(error,message) { console.log('Systemd call failed: ', error, 'message: ', message) }
              )
        }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            PageHeader {
                title: qsTr("DjVu Viewer")
            }

            TextSwitch {
                text: qsTr("Enable viewing of DjVu files")

                description: qsTr("As there is no native support for that file type, I have chosen to do a workaround.  The helper script will convert the djvu temporarily into PDF, and launch Sailfish Office \(a.k.a. \"Documents\"\) to open it. NOTE: this conversion is not fast. You will have to excert some patience for the files to open.\nNot ideal, but it allows you to view djvu files you have or encounter.")
                checked: conf.enabled
                onClicked: {
                  conf.enabled = !conf.enabled
                  function() { console.info("Toggled!") }
                  systemd.runService()
                }
            }
        }

    }//Flickable
}//Page
