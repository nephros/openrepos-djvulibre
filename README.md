
This installs a MIME type handler for DjVu files.

As there is no native support for that file type, I have chosen to do a workaround. The helper script will convert the djvu temporarily into PDF, and launch Sailfish Office (a.k.a. "Documents) to open it.

Not ideal, but it allows you to view djvu files you have or encounter.

**NOTE:** after installation you have to enable the mime type handler in jolla-settings (available in the djvulibre-open-0.3.0 version).

To setup manually run these commands as regular user in the terminal:

    lca-tool --setmimedefault image/vnd.djvu openrepos-djvulibre-open
    lca-tool --setmimedefault image/vnd.djvu+multipage openrepos-djvulibre-open
    lca-tool --setmimedefault image/x.djvu openrepos-djvulibre-open
    lca-tool --setmimedefault image/x-djvu openrepos-djvulibre-open

To avoid typing, you can get the little script from the git repo https://gitlab.com/nephros/openrepos-djvulibre/-/raw/master/files/install-mimes-user.sh


 * Software Site: http://djvu.sourceforge.net
 * Git repo: https://gitlab.com/nephros/openrepos-djvulibre

