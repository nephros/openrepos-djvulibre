#!/usr/bin/env bash

inpath="$1"
[[ -r "${inpath}" ]] || exit 1

# get filename without path
cachedir=${HOME}/.cache/openrepos-djvulibre-open
mkdir -p "${cachedir}"
infilehash=$(sha1sum "${inpath}" | awk '{print $1}')
out="${cachedir}/${infilehash}_djvu.pdf"

if [[ -r "${out}" ]]; then
  echo ${0##*/}: found existing file at: ${out}, opening...
  notificationtool -o add --application="DjVu Helper" --urgency=2 -T 2000 "" "" "Cached converted file found, opening..." ""
  invoker -w --type=silica-qt5 /usr/bin/sailfish-office "$out"
else # we must convert

  # conversion options
  ddjvu_bin=/usr/bin/ddjvu
  ddjvu_opts="-format=pdf -skip"

  ## get screen resolution, use that as resolution
  #h=$(/usr/bin/dconf read /lipstick/screen/primary/height 2>/dev/null)
  #w=$(/usr/bin/dconf read /lipstick/screen/primary/width 2>/dev/null)
  #res=${w}x${h}
  #[[ ! "$res" = "x" ]] && ddjvu_opts="$ddjvu_opts -size=$res"

  # second option: get DPI, use a fraction of them:
  #dpi=$(/usr/bin/dconf read /lipstick/screen/primary/physicalDotsPerInch)
  #dpi=${dpi%.*}
  #ddjvu_scale_factor=1
  #dpi=$(( $dpi / $ddjvu_scale_factor ))
  #[[ ! "$dpi" = "x" ]] && ddjvu_opts="$ddjvu_opts -scale=$dpi"

  # third option: do not use sce scale option at all:
  ddjvu_opts="$ddjvu_opts -1 "

  echo ${0##*/}: converting file using $ddjvu_opts and launching viewer...
  notificationtool -o add --application="DjVu Helper" --urgency=2 -T 2000 "" "" "DjVu conversion started." "This can take some time, please be patient..."
  $ddjvu_bin $ddjvu_opts "$inpath" "$out"
  if [[ $? -ne 0 ]]; then
    notificationtool -o add --application="DjVu Helper" --urgency=2 -T 2000 "" "" "Opening DjVu Failed :("
  else
    invoker -w --type=silica-qt5 /usr/bin/sailfish-office "$out"
  fi
fi
echo ${0##*/}: viewing done, cleaning cache.
find "${cachedir}" -type f -mtime +30 -exec rm {} +
