#!/usr/bin/env bash

appname=openrepos-djvulibre-open
mimes="image/vnd.djvu image/vnd.djvu+multipage	image/x.djvu image/x-djvu"

undo="undef"
conf=$(dconf read "/org/nephros/openrepos-djvulibre/mime-helper/enabled")
case $conf in
  false)
	mode="undo"
	;;
  *)
	mode="do"
	;;
esac

err(){ >&2 echo $@; }

check_mime_installed() {
  local ret
  ret=$(lca-tool --mimedefault ${1})
  if [[ ! -z "$ret" ]] && [[ "${ret}"x = "${appname}"x ]]; then
	echo true
  else
	echo false
  fi
}

reset_mime() {
  #err executing lca-tool --resetmimedefault "${1}"
  lca-tool --resetmimedefault "${1}"
}

install_mime() {
  #err executing lca-tool --setmimedefault "${1}" "${2}"
  lca-tool --setmimedefault "${1}" "${2}"
}

for mime in $mimes;
do
  case $mode in
	undo)
		#err UNDO mode, resetting
		reset_mime "$mime" "$appname"
	;;
	do)
		check=$(check_mime_installed $mime)
		#err check returned: $check for $mime
		[[ "${check}" = "false" ]] && install_mime "$mime" "$appname"
	;;
  esac
done

#always succeed...
exit 0
